package org.palituxd.poc;

import java.util.List;

import org.palituxd.poc.entity.OrgEntity;
import org.palituxd.poc.repository.OrgRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/org")
//@CrossOrigin("http://192.168.99.100:3000")
@CrossOrigin()
public class OrgController {

    @Autowired
    private OrgRepository orgRepository;

    @RequestMapping(path = "/orgs", method = RequestMethod.GET)
    public List<OrgEntity> getOrgs() {
        return orgRepository.findAll();
    }

    @RequestMapping(path = "/orgs1", method = RequestMethod.GET)
    public List<OrgEntity> getOrgs1() {
        return orgRepository.findAll();
    }

    @RequestMapping(path = "/orgs2", method = RequestMethod.GET)
    public List<OrgEntity> getOrgs2() {
        return orgRepository.findAll();
    }

    @RequestMapping(path = "/org/{id}", method = RequestMethod.GET)
    public OrgEntity getOrg(@PathVariable Long id) {
        return orgRepository.findById(id).get();
    }
}
