package org.palituxd.poc.commons;

import lombok.Getter;
import lombok.Setter;
import org.palituxd.poc.entity.Org;

@Getter
@Setter
public class OrgCommon implements Org{
    private Long id;
    private String description;
    private String code;
}
