package org.palituxd.poc.repository;

import java.util.List;

import org.palituxd.poc.entity.OrgEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrgRepository extends JpaRepository<OrgEntity, Long>{

    @Override
    @Query("SELECT c FROM OrgEntity c")
    //@Query("SELECT c FROM CustomerEntity c WHERE c.enabled IS TRUE")
    List<OrgEntity> findAll();
}