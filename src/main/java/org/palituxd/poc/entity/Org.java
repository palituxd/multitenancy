package org.palituxd.poc.entity;

public interface Org {
    Long getId();
    String getDescription();
    String getCode();
}
